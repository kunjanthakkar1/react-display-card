import React, { Component } from "react";
import { Card } from './Card';
import { Popup } from './Popup';
import "./App.css";
import "./Card.css";

class App extends Component {
    constructor() {
      super();
      this.state = {
        showPopup: false,
        currentCard: null,
        frameworks: []
      };

      this.openPopup = this.openPopup.bind(this);
      this.hidePopup = this.hidePopup.bind(this);
      this.deleteCard = this.deleteCard.bind(this);
      this.printCard = this.printCard.bind(this);
    }

  static getDerivedStateFromProps(nextProps, prevState){
    return { frameworks : nextProps.frameworks };
  }
    openPopup(index) {
      this.setState({
        showPopup: true,
        currentCard: index
      })
    }

    hidePopup() {
      this.setState({
        showPopup: false
      })
    }

    deleteCard(index) {
      console.log(index);
      console.log("before delete", this.state.frameworks);
      this.setState({
        frameworks: this.state.frameworks.splice(index, 1)
      })
      console.log("after delete", this.state.frameworks);
    }

    printCard(index) {
       const card = document.getElementById(index);
       card.classList.add("print-box");
       window.print();
       card.classList.remove("print-box");
    }

    render() {
    const { frameworks } = this.state;

    return (
      <div className="container-fluid">
        <div style={styles.title} className="title">
          <img></img>
          <h1><img src={require('./assets/logo.png')} alt="logo" style={styles.logo}/>Welcome to the React Card Display Demo</h1>
          <h3>This demo is built using React.js without any additional UI libraries</h3>
        </div>
        <div className="row card-list">
        {
          frameworks.map(key => <Card key={key.name} index={frameworks.indexOf(key)} details={key} openPopup={this.openPopup} 
            deleteCard={this.deleteCard} printCard={this.printCard}/>)
        }
    </div>
    {this.state.showPopup ? 
          <Popup
            text='Close Me'
            hidePopup={this.hidePopup}
            details={this.state.frameworks[this.state.currentCard]}
          />
          : null
        }
      </div>
    );
  }
};

const styles = {
  title: {
    textAlign: 'center',
    padding: 20,
    backgroundImage: 'url(' + require('./assets/header-background.jpg') + ')',
    backgroundPosition: 'center center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat'
  },
  logo: {
    height: 50,
    verticalAlign: 'text-bottom',
    marginRight: 10
  }
};

export default App;