import React from "react";
import "./Popup.css";

export const Popup = ({ details, hidePopup }) => {
  return (
    <div className='popup'>
      <div className='popup-inner'>
        <h1>{details.name}</h1>
        <button onClick={hidePopup} className="popup-close-button">X</button>
        <img src={details.src} className="popup-image"/>
        <p>{details.text}</p>
      </div>
    </div>
  );
};