import React from "react";
import "./Card.css";

export const CardHeader = ({name}) => {
  return (
    <header className="card-header">
      <h4 className="card-header-title">{name}</h4>
    </header>
  )
};
  
export const CardBody = ({src}) => {
  const styles = { 
    body: {
      backgroundImage: 'url(' + src + ')',
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'contain',
      backgroundPosition: 'center'
    }
  };

  return (
    <div className="card-body" style={styles.body}></div>
  )
};
  
  export const OptionMenu = ({index, openPopup, deleteCard, printCard}) => {
    return (
      <div className="option-menu">
        <a onClick={() => openPopup(index)} href="#">Open</a>
        <a onClick={() => deleteCard(index)} href="#">Delete</a>
        <a onClick={() => printCard(index)} href="#">Print</a>
      </div>
    )
  };

  export const Card = ({details, index, openPopup, deleteCard, printCard}) => {
    return (
      <div className="col-xs-1 col-sm-6 col-md-4 card">
        <CardHeader name={details.name}/>
        <CardBody src={details.src}/>
        <OptionMenu index={index} openPopup={openPopup} deleteCard={deleteCard} printCard={printCard}/>
      </div>
    )
  };