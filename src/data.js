module.exports = {
    frameworks: [
      { name: "Javascript", src: require('./assets/JS.png'), 
        text: "JavaScript often abbreviated as JS, is a high-level, interpreted programming language. It is a language which is also characterized as dynamic, weakly typed, prototype-based and multi-paradigm.Alongside HTML and CSS, JavaScript is one of the three core technologies of the World Wide Web. JavaScript enables interactive web pages and thus is an essential part of web applications. The vast majority of websites use it, and all major web browsers have a dedicated JavaScript engine to execute it."},
      { name: "Angular", src: require('./assets/angular.png'), 
        text: "Angular (commonly referred to as Angular 2+ or Angular v2 and above) is a TypeScript-based open-source front-end web application platform led by the Angular Team at Google and by a community of individuals and corporations. Angular is a complete rewrite from the same team that built AngularJS."},
      { name: "React", src: require('./assets/react.png'),
        text: "React (also known as React.js or ReactJS) is a JavaScript library for building user interfaces. It is maintained by Facebook and a community of individual developers and companies. React can be used as a base in the development of single-page or mobile applications. Complex React applications usually require the use of additional libraries for state management, routing, and interaction with an API."},
      { name: "Vue", src: require('./assets/vue.png'),
        text: "Vue.js (commonly referred to as Vue) is an open-source JavaScript framework for building user interfaces and single-page applications. Vue.js features an incrementally adoptable architecture that focuses on declarative rendering and component composition."},
      { name: "Ember", src: require('./assets/ember.png'),
        text: "Ember.js is an open-source JavaScript web framework, based on the Model–view–viewmodel (MVVM) pattern. It allows developers to create scalable single-page web applications by incorporating common idioms and best practices into the framework."},
      { name: "Meteor", src: require('./assets/meteor.png'),
        text: "Meteor, or MeteorJS, is a free and open-source isomorphic JavaScript web framework written using Node.js. Meteor allows for rapid prototyping and produces cross-platform (Android, iOS, Web) code. On the client, Meteor can be used with its own Blaze templating engine, as well as with the Angular or React frameworks."},
      { name: "Polymer", src: require('./assets/polymer.png'),
        text: "Polymer is an open-source JavaScript library for building web applications using polymer Web Components. The library is being developed by Google developers and contributors on GitHub. Modern design principles are implemented as a separate project using Google's Material Design design principles."},
      { name: "Dojo", src: require('./assets/dojo.jpeg'),
        text: "Dojo Toolkit is an open-source modular JavaScript library (or more specifically JavaScript toolkit) designed to ease the rapid development of cross-platform, JavaScript/Ajax-based applications and web sites."},
      { name: "Ext JS", src: require('./assets/extjs.jpeg'),
        text: "Ext JS is a pure JavaScript application framework for building interactive cross platform web applications using techniques such as Ajax, DHTML and DOM scripting. It can be used as a simple component framework, but also as a full framework for building single-page applications."},
      { name: "jQuery", src: require('./assets/jquery.png'),
        text: "jQuery is a JavaScript library designed to simplify HTML DOM tree traversal and manipulation, as well as event handling, animation, and Ajax. It is free, open-source software using the permissive MIT License. Web analysis indicates that it is the most widely deployed JavaScript library by a large margin."},
      { name: "Knockout", src: require('./assets/knockout.png'),
        text :"Knockout is a standalone JavaScript implementation of the Model-View-ViewModel pattern with templates. It has a a clear separation between domain data, view components and data to be displayed"},
      { name: "Backbone", src: require('./assets/backbone.png'),
        text: "Backbone.js is a JavaScript library with a RESTful JSON interface and is based on the Model–view–presenter (MVP) application design paradigm. Backbone is known for being lightweight, as its only hard dependency is on one JavaScript library, Underscore.js, plus jQuery for use of the full library."}
    ]
}