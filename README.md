# react-display-card

## Description
This is a web app built using React.js It has the following features/functionalities:
1. Header with a title, subtitle, logo and an image background
2. Table of cards representing different JS frameworks with title and logo for each card
3. Upon hovering on any card, you will be presented with three options - Open, Delete and Print
    a. Open will open a popup with the name, logo and a short description of the framework represented by the card
    b. Delete will delete the card from the list 
    c. Print will trigger a print of the name and logo of the framework represented by the card
4. Responsiveness is achieved using bootstrap grid v3

## Setup, build and run
1. npm i
2. npm start 
